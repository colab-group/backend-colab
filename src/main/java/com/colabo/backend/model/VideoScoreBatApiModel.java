package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VideoScoreBatApiModel {
	
	String title;
	String embed;
	
	public VideoScoreBatApiModel(
			@JsonProperty("title") String title, 
			@JsonProperty("embed") String embed) {
		this.title = title;
		this.embed = this.getUrlFromEmbedHtml(embed);
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("embed")
	public String getEmbed() {
		return embed;
	}
	
	private String getUrlFromEmbedHtml(String embedHtmlVideo) {
		String urlAvecFinDeBalise = embedHtmlVideo.split("src='")[1];
		return urlAvecFinDeBalise.split("'")[0];
	}	

}
