package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MusicDeezerApiModel {

	Integer id;
	Boolean readable;
	String title;
	String title_short;
	String title_version;
	String link;
	Integer duration;
	Integer rank;
	Boolean explicit_lyrics;
	Integer explicit_content_lyrics;
	Integer explicit_content_cover;
	String preview;
	DeezerArtist artist;
	DeezerAlbum album;
	String type;
	

	@JsonCreator
	public MusicDeezerApiModel(
			@JsonProperty("id") Integer id,
			@JsonProperty("readable") Boolean readable,
			@JsonProperty("title") String title,
			@JsonProperty("title_short") String title_short,
			@JsonProperty("title_version") String title_version,
			@JsonProperty("link") String link,
			@JsonProperty("duration") Integer duration,
			@JsonProperty("rank") Integer rank,
			@JsonProperty("explicit_lyrics") Boolean explicit_lyrics,
			@JsonProperty("explicit_content_lyrics") Integer explicit_content_lyrics,
			@JsonProperty("explicit_content_cover") Integer explicit_content_cover,
			@JsonProperty("preview") String preview,
			@JsonProperty("artist") DeezerArtist artist,
			@JsonProperty("album") DeezerAlbum album,
			@JsonProperty("type") String type
			){
		
		this.id = id;
		this.readable = readable;
		this.title = title;
		this.title_short = title_short;
		this.title_version = title_version;
		this.link = link;
		this.duration = duration;
		this.rank = rank;
		this.explicit_lyrics = explicit_lyrics;
		this.explicit_content_lyrics = explicit_content_lyrics;
		this.explicit_content_cover = explicit_content_cover;
		this.preview = preview;
		this.artist = artist;
		this.album = album;
		this.type = type;
	}
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("readable")
	public Boolean getReadable() {
		return readable;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title_short")
	public String getTitle_short() {
		return title_short;
	}

	@JsonProperty("title_version")
	public String getTitle_version() {
		return title_version;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	@JsonProperty("duration")
	public Integer getDuration() {
		return duration;
	}

	@JsonProperty("rank")
	public Integer getRank() {
		return rank;
	}

	@JsonProperty("explicit_lyrics")
	public Boolean getExplicit_lyrics() {
		return explicit_lyrics;
	}

	@JsonProperty("explicit_content_lyrics")
	public Integer getExplicit_content_lyrics() {
		return explicit_content_lyrics;
	}

	@JsonProperty("explicit_content_cover")
	public Integer getExplicit_content_cover() {
		return explicit_content_cover;
	}

	@JsonProperty("preview")
	public String getPreview() {
		return preview;
	}

	@JsonProperty("artist")
	public DeezerArtist getArtist() {
		return artist;
	}

	@JsonProperty("album")
	public DeezerAlbum getAlbum() {
		return album;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}
	

}
