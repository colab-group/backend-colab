package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeezerArtist {
	Integer id;
	String name;
	String link;
	String picture;
	String picture_small;
	String picture_medium;
	String picture_big;
	String picture_xl;
	String tracklist;
	String type;
	
	@JsonCreator
	public DeezerArtist(
			@JsonProperty("id") Integer id,
			@JsonProperty("name") String name,
			@JsonProperty("link") String link,
			@JsonProperty("picture") String picture,
			@JsonProperty("picture_small") String picture_small,
			@JsonProperty("picture_medium") String picture_medium,
			@JsonProperty("picture_big") String picture_big,
			@JsonProperty("picture_xl") String picture_xl,
			@JsonProperty("tracklist") String tracklist,
			@JsonProperty("type") String type
			){
		
		this.id = id;
		this.name=name;
		this.link = link;
		this.picture = picture;
		this.picture_small = picture_small;
		this.picture_medium = picture_medium;
		this.picture_big = picture_big;
		this.picture_xl = picture_xl;
		this.tracklist = tracklist;
		this.type = type;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	@JsonProperty("picture")
	public String getPicture() {
		return picture;
	}

	@JsonProperty("picture_small")
	public String getPicture_small() {
		return picture_small;
	}

	@JsonProperty("picture_medium")
	public String getPicture_medium() {
		return picture_medium;
	}

	@JsonProperty("picture_big")
	public String getPicture_big() {
		return picture_big;
	}


	@JsonProperty("picture_xl")
	public String getPicture_xl() {
		return picture_xl;
	}

	@JsonProperty("tracklist")
	public String getTracklist() {
		return tracklist;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	
	
}
