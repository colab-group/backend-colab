package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeezerAPIModel {
	
	MusicDeezerApiModel [] deezerMusicItems;
	Integer total;
	String next;
	
	@JsonCreator
	public DeezerAPIModel(@JsonProperty("data") MusicDeezerApiModel [] deezerMusicItems,@JsonProperty("total") Integer total, @JsonProperty("next") String next ) {
		
		this.deezerMusicItems = deezerMusicItems;
	}
	
	@JsonProperty("data")
	public MusicDeezerApiModel [] getDeezerMusicItems() {
		return deezerMusicItems;
	}
	
	@JsonProperty("total")
	public Integer getTotal(){
		return total;
	}
	
	@JsonProperty("next")
	public String getNext() {
		return next;
	}
	

}
