package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyMotionApiModel {


	Integer page;
	Integer limit;
	Boolean explicit;
	Integer total;
	Boolean has_more;
	VideoDailymotionApiModel[] list;
	
	
	@JsonCreator
	public DailyMotionApiModel(
			@JsonProperty("page") Integer page,
			@JsonProperty("limit") Integer limit,
			@JsonProperty("explicit") Boolean explicit,
			@JsonProperty("total") Integer total,
			@JsonProperty("has_more") Boolean has_more,
			@JsonProperty("list") VideoDailymotionApiModel[] list) {
		
		this.page = page;
		this.limit = limit;
		this.explicit = explicit;
		this.total = total;
		this.has_more = has_more;
		this.list = list;
	}


	@JsonProperty("page")
	public Integer getPage() {
		return page;
	}

	@JsonProperty("limit")
	public Integer getLimit() {
		return limit;
	}

	@JsonProperty("explicit")
	public Boolean getExplicit() {
		return explicit;
	}


	@JsonProperty("total")
	public Integer getTotal() {
		return total;
	}


	@JsonProperty("has_more")
	public Boolean getHas_more() {
		return has_more;
	}


	@JsonProperty("list")
	public VideoDailymotionApiModel[] getList() {
		return list;
	}
	
	 
}
