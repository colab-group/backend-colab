package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VideoDailymotionApiModel {

	String id;
	String title;
	String channel;
	String owner;
	
	
	@JsonCreator
	public VideoDailymotionApiModel(
			@JsonProperty("id") String id,
			@JsonProperty("title") String title,
			@JsonProperty("channel") String channel,
			@JsonProperty("owner") String owner) {
		
		this.id = "https://www.dailymotion.com/embed/video/"+id;
		this.title = title;
		this.channel = channel;
		this.owner = owner;
	}


	@JsonProperty("id")
	public String getId() {
		return id;
	}


	@JsonProperty("title")
	public String getTitle() {
		return title;
	}


	@JsonProperty("channel")
	public String getChannel() {
		return channel;
	}


	@JsonProperty("owner")
	public String getOwner() {
		return owner;
	}
	
}
