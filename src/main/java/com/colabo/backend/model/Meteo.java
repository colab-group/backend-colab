package com.colabo.backend.model;

public enum Meteo {
	Thunderstorm("11d,11n"),
	Drizzle("09d,09n"),
	Rain("10d,10n"),
	Snow("13d,13n"),
	Atmosphere("50d,50n"),
	Clear("01n,01d"),
	Clouds("02d,03d,04d,02n,03n,04n");
	
	private String type = "";
	
	Meteo(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return type;
	}
}