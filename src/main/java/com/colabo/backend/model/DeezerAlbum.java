package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DeezerAlbum {
	
	Integer id;
	String title;
	String cover;
	String cover_small;
	String cover_medium;
	String cover_big;
	String cover_xl;
	String tracklist;
	String type;
	
	@JsonCreator
	public DeezerAlbum(
			@JsonProperty("id") Integer id,
			@JsonProperty("title") String title,
			@JsonProperty("cover") String cover,
			@JsonProperty("cover_small") String cover_small,
			@JsonProperty("cover_medium") String cover_medium,
			@JsonProperty("cover_big") String cover_big,
			@JsonProperty("cover_xl") String cover_xl,
			@JsonProperty("tracklist") String tracklist,
			@JsonProperty("type") String type
			){
		
		this.id = id;
		this.title = title;
		this.cover = cover;
		this.cover_small = cover_small;
		this.cover_medium = cover_medium;
		this.cover_big = cover_big;
		this.cover_xl = cover_xl;
		this.tracklist = tracklist;
		this.type = type;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("cover")
	public String getCover() {
		return cover;
	}

	@JsonProperty("cover_small")
	public String getCover_small() {
		return cover_small;
	}

	@JsonProperty("cover_medium")
	public String getCover_medium() {
		return cover_medium;
	}

	@JsonProperty("cover_big")
	public String getCover_big() {
		return cover_big;
	}


	@JsonProperty("cover_xl")
	public String getCover_xl() {
		return cover_xl;
	}

	@JsonProperty("tracklist")
	public String getTracklist() {
		return tracklist;
	}


	@JsonProperty("type")
	public String getType() {
		return type;
	}

	
}
