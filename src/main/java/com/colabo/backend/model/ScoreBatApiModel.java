package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ScoreBatApiModel {

	
	String title;
	String embed;
	String url;
	String thumbnail;
	String date;
	VideoScoreBatApiModel[] videos;
	Object sideOne;
	Object sideTwo;
	Object competition;
	
	@JsonCreator
	public ScoreBatApiModel(
			@JsonProperty("title") String title,
			@JsonProperty("url") String url,
			@JsonProperty("thumbnail") String thumbnail,
			@JsonProperty("date") String date,
			@JsonProperty("videos") VideoScoreBatApiModel[] videos) {
		
		this.title = title;
		this.url = url;
		this.thumbnail = thumbnail;
		this.date = date;
		this.videos = videos;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("embed")
	public String getEmbed() {
		return embed;
	}
	
	@JsonProperty("url")
	public String getUrl() {
		return url;
	}

	@JsonProperty("thumbnail")
	public String getThumbnail() {
		return thumbnail;
	}

	@JsonProperty("videos")
	public VideoScoreBatApiModel[] getVideo() {
		return videos;
	}
	
	@JsonProperty("side1")
	public Object getSideOne() {
		return sideOne;
	}
	
	@JsonProperty("side2")
	public Object getSideTwo() {
		return sideTwo;
	}
	
	@JsonProperty("competition")
	public Object getCompetition() {
		return competition;
	}
	
	
	
	
}
