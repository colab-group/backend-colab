package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(value = { "rain" })
public class OpenWeatherApiModel {

	@JsonProperty
	WeatherOpenWeatherMap[] weather;	
	@JsonProperty
	Object coord;	
	@JsonProperty
	String base ;
	@JsonProperty 
	Object main;
	@JsonProperty
	Integer visibility;
	@JsonProperty
	Object wind;
	@JsonProperty
	Object clouds;
	@JsonProperty
	Integer dt;
	@JsonProperty
	Object sys;
	@JsonProperty
	Integer timezone;
	@JsonProperty
	Integer id;
	@JsonProperty
	String name;
	@JsonProperty
	Integer cod;
	
	
	@JsonCreator
	public OpenWeatherApiModel(
			
			@JsonProperty("coord") Object coord,
			@JsonProperty("weather") WeatherOpenWeatherMap[] list,
			@JsonProperty("base") String base,
			@JsonProperty("main") Object main,
			@JsonProperty("visibility") Integer visibility,
			@JsonProperty("wind") Object wind,
			@JsonProperty("clouds") Object clouds,
			@JsonProperty("dt") Integer dt,
			@JsonProperty("sys") Object sys,
			@JsonProperty("timezone") Integer timezone,
			@JsonProperty("id") Integer id,
			@JsonProperty("name") String name,
			@JsonProperty("cod") Integer cod) {		
		this.weather = list;
		this.coord = coord;
		this.base = base;
		this.main = main;
		this.visibility = visibility;
		this.wind = wind;
		this.clouds = clouds;
		this.dt = dt;
		this.sys = sys;
		this.timezone = timezone;
		this.id = id;
		this.name = name;
		this.cod = cod;
	}

	@JsonGetter
	public Object getCoord() {
		return coord;
	}

	@JsonProperty("coord")
	public void setCoord(Object coord) {
		this.coord = coord;
	}

	@JsonGetter
	public String getBase() {
		return base;
	}

	@JsonProperty("base")
	public void setBase(String base) {
		this.base = base;
	}

	@JsonGetter
	public Object getMain() {
		return main;
	}

	@JsonProperty("main")
	public void setMain(Object main) {
		this.main = main;
	}

	@JsonGetter
	public Integer getVisibility() {
		return visibility;
	}

	@JsonProperty("visibility")
	public void setVisibility(Integer visibility) {
		this.visibility = visibility;
	}

	@JsonGetter
	public Object getWind() {
		return wind;
	}

	@JsonProperty("wind")
	public void setWind(Object wind) {
		this.wind = wind;
	}

	@JsonGetter
	public Object getClouds() {
		return clouds;
	}

	@JsonProperty("clouds")
	public void setClouds(Object clouds) {
		this.clouds = clouds;
	}

	@JsonGetter
	public Integer getDt() {
		return dt;
	}

	@JsonProperty("dt")
	public void setDt(Integer dt) {
		this.dt = dt;
	}

	@JsonGetter
	public Object getSys() {
		return sys;
	}

	@JsonProperty("sys")
	public void setSys(Object sys) {
		this.sys = sys;
	}

	@JsonGetter
	public Integer getTimezone() {
		return timezone;
	}

	@JsonProperty("timezone")
	public void setTimezone(Integer timezone) {
		this.timezone = timezone;
	}

	@JsonGetter
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonGetter
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonGetter
	public Integer getCod() {
		return cod;
	}

	@JsonProperty("cod")
	public void setCod(Integer cod) {
		this.cod = cod;
	}

	@JsonProperty("weather")
	public WeatherOpenWeatherMap[] getWeather() {
		return weather;
	}


	@JsonProperty("weather")
	public void setWeather(WeatherOpenWeatherMap[] weather) {
		this.weather = weather;
	}


	
	
	 
}
