package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WeatherOpenWeatherMap {

	@JsonProperty
	Integer id;
	
	@JsonProperty
	String main;
	
	@JsonProperty
	String description;
	
	@JsonProperty
	String icon;
	
	public WeatherOpenWeatherMap(
			@JsonProperty("id") Integer id,
			@JsonProperty("main") String main,
			@JsonProperty("description") String description,
			@JsonProperty("icon") String icon) {
		
		this.id = id;
		this.main = main;
		this.description = description;
		this.icon = icon;
	}
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("main")
	public String getMain() {
		return main;
	}

	@JsonProperty("main")
	public void setMain(String main) {
		this.main = main;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("icon")
	public String getIcon() {
		return icon;
	}

	@JsonProperty("icon")
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
}
