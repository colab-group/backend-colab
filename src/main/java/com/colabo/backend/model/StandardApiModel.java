package com.colabo.backend.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class StandardApiModel {

	VideoDailymotionApiModel [] dailymotionVideo;
	OpenWeatherApiModel openWeather;
	DeezerAPIModel deezerMusic;
	
	
	
	@JsonCreator
	public StandardApiModel(@JsonProperty("dailymotion") VideoDailymotionApiModel[] dailymotionVideo, 
							@JsonProperty("deezer") DeezerAPIModel deezerMusic,
							@JsonProperty("openweather") OpenWeatherApiModel openWeather) {
		
		this.dailymotionVideo = dailymotionVideo;
		this.openWeather = openWeather;			 
		this.deezerMusic = deezerMusic;
	}
	
	@JsonProperty("dailymotion")
	public VideoDailymotionApiModel[] getDailymotionVideo() {
		return dailymotionVideo;
	}
	
	@JsonProperty("deezer")
	public DeezerAPIModel getDeezerMusic() {
		return deezerMusic;
	}

	@JsonProperty("dailymotion")
	public void setDailymotionVideo(VideoDailymotionApiModel[] dailymotionVideo) {
		this.dailymotionVideo = dailymotionVideo;
	}

	@JsonProperty("openweather")
	public OpenWeatherApiModel getOpenWeather() {
		return openWeather;
	}

	@JsonProperty("openweather")
	public void setOpenWeather(OpenWeatherApiModel openWeather) {
		this.openWeather = openWeather;
	}
	
	
	@JsonProperty("deezer")
	public void setDeezerMusic(DeezerAPIModel deezerApi) {
		this.deezerMusic = deezerApi;
	}
	
	
}
