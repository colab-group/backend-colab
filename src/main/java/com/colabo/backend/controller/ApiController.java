package com.colabo.backend.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.colabo.backend.model.StandardApiModel;
import com.colabo.backend.service.ApiService;


@RestController
public class ApiController {
	
	@Autowired
	ApiService responseService;
	
	@Value("${properties.apis}")
	private String apis;
	

	@RequestMapping(value = "/api")
	 public @ResponseBody StandardApiModel getAllDataFromApis() throws IOException  {
		return this.responseService.getStandardApiData();
	 }
	
}	
