package com.colabo.backend.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.colabo.backend.model.DailyMotionApiModel;
import com.colabo.backend.model.OpenWeatherApiModel;
import com.colabo.backend.model.StandardApiModel;
import com.colabo.backend.model.VideoDailymotionApiModel;
import com.colabo.backend.model.WeatherOpenWeatherMap;
import com.colabo.backend.model.DeezerAPIModel;
import com.colabo.backend.model.Meteo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ApiService {
	
	@Value("${properties.tags}")
	private String[] tags;
	

	@Value("${properties.apis}")
	private String apis;
	
	@Value("${properties.town}")
	private String town;
	
	@Value("${properties.country}")
	private String country;
	
	@Value("${properties.tagsbyweather}")
	private Boolean isTagsByWeatherSelected;
	
	// weather condition → https://openweathermap.org/weather-conditions
	
	private final static String DAILYMOTION_DEFAULT_ROUTE = "https://api.dailymotion.com/videos?";
	private final static String DEEZER_DEFAULT_ROUTE = "https://api.deezer.com/search?q="; // &index=25
	private String open_weather_map_api = "https://api.openweathermap.org/data/2.5/weather?q=";
	private final static ArrayList<String> champsLexicalMeteo = new ArrayList<>();
	
	
	/**
	 * This method build the api by calling deezer, dailymotion, openweathermap api
	 * @return the api standardized 
	 * @throws IOException
	 */
	public StandardApiModel getStandardApiData() throws IOException {
		StandardApiModel standardApiValue = new StandardApiModel(null,null,null);
		WeatherOpenWeatherMap openWeatherMap;
		
		standardApiValue.setOpenWeather(this.getDataFromOpenWeather());
		if (isTagsByWeatherSelected) {
						
			// get the first element of the list weather
			openWeatherMap = standardApiValue.getOpenWeather().getWeather()[0];
			
			if (Meteo.Atmosphere.toString().contains(openWeatherMap.getIcon())) {
				//Cas brouillard
				InitChampLexicalBrouillard();
			}
			
			if (Meteo.Clear.toString().contains(openWeatherMap.getIcon())) {
				//Cas soleil
				InitChampLexicalSoleil();
			}
			
			if (Meteo.Clouds.toString().contains(openWeatherMap.getIcon())) {
				//Cas nuageux
				InitChampLexicalNuageux();
			}
			
			if (Meteo.Drizzle.toString().contains(openWeatherMap.getIcon())) {
				//Cas bruine
				InitChampLexicalBruine();
			}
			
			if (Meteo.Rain.toString().contains(openWeatherMap.getIcon())) {
				//Cas pluie
				InitChampLexicalPluie(); // remplissage de la liste concernant la pluie
			}
			
			if (Meteo.Rain.toString().contains(openWeatherMap.getIcon())) {
				//Cas neige
				InitChampLexicalNeige();
			}
			
			if (Meteo.Thunderstorm.toString().contains(openWeatherMap.getIcon())) {
				//Cas orage
				InitChampLexicalOrage();
			}
		}

		if (apis.contains("dailymotion")) {
			standardApiValue.setDailymotionVideo(this.getAllVideoFromDailyMotionApi());
		 }

		if (apis.contains("deezer")) {
			standardApiValue.setDeezerMusic(this.getDataFromDeezer());
		}
		return standardApiValue;
	}
	
	/**
	 * This method can call dailymotion api
	 * and build the final api body
	 * @return the api body formated for the field dailymotion
	 * @throws IOException
	 */
	private DailyMotionApiModel getDataFromDailymotion() throws IOException {
		RestTemplate restTmplt = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String response = restTmplt.getForObject(this.buildRouteForDailyMotion(), String.class);
		return mapper.readValue(response, DailyMotionApiModel.class);
	}
	
	/**
	 * This method can call openweathermap api
	 * and build the final api body
	 * @return the api body formated for the field openweathermap 
	 * @throws IOException
	 */
	private OpenWeatherApiModel getDataFromOpenWeather() throws IOException {
		String url = this.open_weather_map_api+town+","+country+"&appid=f30273e7f45a4bdedb56e94acaa1f0ff";
		RestTemplate restTmplt = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String response = restTmplt.getForObject(url, String.class);
		return mapper.readValue(response, OpenWeatherApiModel.class);
	}

	/**
	 * This method can call deezer api
	 * and build the final api body
	 * @return the api body formated for the field deezer 
	 * @throws IOException
	 */
	private DeezerAPIModel getDataFromDeezer() throws IOException {
		RestTemplate restTmplt = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String response = restTmplt.getForObject(this.buildRouteForDeezer(), String.class);
		return mapper.readValue(response, DeezerAPIModel.class);  
	}

	/**
	 * Cette methode permet de creer la route complete pour dailymotion
	 * */
	private String buildRouteForDailyMotion() {
		return this.DAILYMOTION_DEFAULT_ROUTE+"tags="+this.getTagsPath()+this.generateTagByWeather();
	}

	/**
	 * Cette methode permet de creer la route complete pour deezer
	 * */
	private String buildRouteForDeezer() {
		
		return this.DEEZER_DEFAULT_ROUTE+this.getTagsPath()+","+this.generateTagByWeather();
	}
	
	
	/**
	 * cette methode permet d'ajouter les tags dans la route de dailymotion et de deezer
	 * */
	private String getTagsPath() {
		String tagsForRouteApi = "";
		
		for (String tag: tags) {           
			tagsForRouteApi += tag+",";  
		}
		tagsForRouteApi = removeLastChar(tagsForRouteApi);
		
		return tagsForRouteApi;
	}
	
	
	/**
	 * Cette méthode permet de retirer le dernier caractere d'une chaine de caractere
	 * */
	private String removeLastChar(String str) {
	    return str.substring(0, str.length() - 1);
	}

	/**
	 * Cette methode permet de récuperer les valeurs de l'api Dailymotion
	 * */
	private VideoDailymotionApiModel[] getAllVideoFromDailyMotionApi() throws IOException {
		ArrayList<VideoDailymotionApiModel> videos = new ArrayList<>();
		DailyMotionApiModel dailymotionApi = this.getDataFromDailymotion();
		for (VideoDailymotionApiModel item :dailymotionApi.getList()) {
			videos.add(item);
		}
		Object[] objectList = videos.toArray();
		return Arrays.copyOf(objectList,objectList.length,VideoDailymotionApiModel[].class);
	}
	
	/**
	 * This method can add the field for the field Pluie 
	 */
	private void InitChampLexicalPluie() {
		champsLexicalMeteo.add("pluie");
		champsLexicalMeteo.add("tristesse");
	}
	
	/**
	 * This method can add the field for the field Brouillard 
	 */
	private void InitChampLexicalBrouillard() {
		champsLexicalMeteo.add("scary");
		champsLexicalMeteo.add("angoisse");
	}
	
	/**
	 * This method can add the field for the field Soleil 
	 */
	private void InitChampLexicalSoleil() {
		champsLexicalMeteo.add("summer");
		champsLexicalMeteo.add("fiesta");
	}
	
	/**
	 * This method can add the field for the field Nuageux 
	 */
	private void InitChampLexicalNuageux() {
		champsLexicalMeteo.add("nuage");
		champsLexicalMeteo.add("tristesse");
	}
	
	/**
	 * This method can add the field for the field Bruine 
	 */
	private void InitChampLexicalBruine() {
		champsLexicalMeteo.add("melancolie");
		champsLexicalMeteo.add("automne");
	}
	
	/**
	 * This method can add the field for the field Neige 
	 */
	private void InitChampLexicalNeige() {
		champsLexicalMeteo.add("noel");
		champsLexicalMeteo.add("ski");
	}

	/**
	 * This method can add the field for the field Neige 
	 */
	private void InitChampLexicalOrage() {
		champsLexicalMeteo.add("eclair");
		champsLexicalMeteo.add("desespoir");
	}
	
	/**
	 * This function choose one field among the list of 
	 * tag by weather 
	 * @return a string with the word generate
	 */
	private String generateTagByWeather() {
		String returnedValue = "";
		Random rnd = new Random();
		
		if(isTagsByWeatherSelected) {
			int nbRnd = 0;
			String tagsByWeather = "";
			nbRnd = rnd.nextInt(champsLexicalMeteo.size());
			tagsByWeather += champsLexicalMeteo.get(nbRnd)+",";
			returnedValue = ","+ removeLastChar(tagsByWeather);
		} else {
			returnedValue = ",";
			returnedValue = removeLastChar(returnedValue);
		}
		return returnedValue ;
	}
}

