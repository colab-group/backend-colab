FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/colab-backend.jar
ENV PORT 8080

EXPOSE ${PORT}

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-Dserver.port=${PORT}","-jar","/app.jar"]
